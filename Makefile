CC=mpicc
#MPI=mpicc
CFLAGS=-g

OUT=obj
BIN=bin

all: main
	echo "Rule all works" 

main: prebuild compile
	$(CC) -o $(BIN)/main_parallel $(OUT)/*.o  	

compile:	
	$(CC) $(CFLAGS) -c src/oblig2_parallel.c -o $(OUT)/oblig2_parallel.o
	$(CC) $(CFLAGS) -c src/matrix_init.c -o $(OUT)/matrix_init.o
	$(CC) $(CFLAGS) -c src/matrix_math.c -o $(OUT)/matrix_math.o
	$(CC) $(CFLAGS) -c src/matrix_tools.c -o $(OUT)/matrix_tools.o

prebuild:
	mkdir -p obj;
	mkdir -p bin;

clean:
	rm -r obj;
	rm -r bin;
