#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

#define DEBUG 1

#ifdef DEBUG
#define d printf
#else
#define d
#endif


#include "../include/matrix_init.h"
#include "../include/matrix_math.h"
#include "../include/matrix_tools.h"
#define BLOCKS 999
int main(int argc, char* argv[]){
	double **A;
	double **B;
	double **C;
	double **Q;
	double *x;
	double *y;
	int a_num_rows;
	int b_num_rows;
	int a_num_cols;
	int b_num_cols;
	int my_rank;
	int num_procs;
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
	MPI_Comm_size(MPI_COMM_WORLD,&num_procs);
	MPI_Status status;
	if(my_rank==0){
		/*char *input1=argv[1];
		char *input2=argv[2];
		read_matrix_binaryformat (input1, &A, &a_num_rows, &a_num_cols);	//100x50
		read_matrix_binaryformat (input2, &B, &b_num_rows, &b_num_cols);	//50x100*/
		a_num_rows=atoi(argv[1]);
		a_num_cols=a_num_rows;	
		b_num_rows=atoi(argv[2]);
		b_num_cols=b_num_rows;
	
	}
	MPI_Bcast(&a_num_rows,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&a_num_cols,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&b_num_rows,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&b_num_cols,1,MPI_INT,0,MPI_COMM_WORLD);

	  alloc(&A,&a_num_rows,&a_num_cols);
	  alloc(&B,&b_num_rows,&b_num_cols);

	  allocBlock(&Q,&b_num_rows,&b_num_cols,num_procs);
	  alloc(&C,&a_num_rows,&a_num_cols);
	  fill2D(A,B,a_num_rows,a_num_cols);
	alloc(&C,&a_num_rows,&b_num_cols);
	alloc1D(&x,a_num_cols);
	alloc1D(&y,a_num_cols);
	fill1D(x,a_num_cols);
	
	 if(my_rank==0){
	 	int i;
		int k;
		int j=0;	// j = 2 fordi hopper over første blokk ={0,1,4,5}
		int n = a_num_rows;
		for(k=1;k<num_procs;k++){
		 	//for(i=0;i<n;i++){	// n / sqrt(num_procs) = n/2...
			 	//sender sender de 2 første radene til første blokk...
				d("sending index = %d\n",j);
				//d("sending index = %d\n",j+n);
			  	MPI_Send(x, 4, MPI_DOUBLE, k, BLOCKS, MPI_COMM_WORLD);	
				//sender sender de 2 første radene til andre blokk...
		  		//MPI_Send(x+j+n, 1, MPI_DOUBLE, k, BLOCKS, MPI_COMM_WORLD);
				j++;
			//}
			/*if(j%num_procs==0)	//ferdig med en blokk rad dersom ingen rest
				j+=n;	//hopper over den rad som aledrede er tatt*/
		}
	  }
	else{
		int i,j;
		int n = a_num_rows;
		int count=0;
		//d("proc %d recv \n",my_rank);
		//for(i=1;i<num_procs;i++){
				MPI_Probe(0,BLOCKS,MPI_COMM_WORLD,&status);
				MPI_Get_count(&status, MPI_DOUBLE, &count);
				d("count=%d,source=%d,tag=%d\n",count,status.MPI_SOURCE,status.MPI_TAG);
				MPI_Recv(y, 4, MPI_DOUBLE, 0, BLOCKS, MPI_COMM_WORLD, &status);
				d("pre-recv\n");
		//}
//		d("y[0]=%f\n",y[0]);
	}
	if(my_rank!=0){
		int i;
		int n = a_num_rows;
		for(i=0;i<n;i++)
				d("y[%d]=%f\n",i,y[i]);
	}


	if(my_rank==0){
		d("A = \n");
		printMatrix(A,a_num_rows,a_num_cols);
		d("x = \n");
		printVector(x,a_num_rows);
		clock_t start = clock(), diff;
		int m=a_num_rows;
		int n=b_num_cols;
		int l=a_num_cols;
		//multiply(A,B,C,m,n,l);
		multiplyVector(A,x,y,a_num_rows,a_num_cols);
		d("y = \n");
		printVector(y,a_num_rows);
		
		diff = clock() - start;

		int msec = diff * 1000 / CLOCKS_PER_SEC;
		printf("Time taken %d seconds %d milliseconds\n", msec/1000, msec%1000);
	}

	d("her med proc %d\n",my_rank);
	MPI_Finalize();
	return 0;
}
