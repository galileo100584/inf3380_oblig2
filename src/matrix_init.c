#include <stdlib.h>
void alloc(double*** matrix, int *num_rows, int *num_cols){
        int i;
        *matrix = (double**)malloc((*num_rows)*sizeof(double*));
        (*matrix)[0] = (double*)malloc((*num_rows)*(*num_cols)*sizeof(double));
        for (i=1; i<(*num_rows); i++)
                (*matrix)[i] = (*matrix)[i-1]+(*num_cols);
}
void allocBlock(double*** matrix, int *num_rows, int *num_cols, int num_procs){
        int i;
        *matrix = (double**)malloc((*num_rows/num_procs)*sizeof(double*));
        (*matrix)[0] = (double*)malloc((*num_rows/num_procs)*(*num_cols)*sizeof(double));
        for (i=1; i<(*num_rows/num_procs); i++)
                (*matrix)[i] = (*matrix)[i-1]+(*num_cols);
}
void alloc1D(double** vector, int num_rows){
        int i;
		double *v;
        v = malloc((num_rows)*sizeof(double*));
		*vector=v;
}
void fill2D(double** A, double** B, int num_rows, int num_cols){
        int i,j;
        int k=0;
        for(i=0;i<num_rows;i++)
                for(j=0;j<num_cols;j++){
                        A[i][j]=(double)k;
                        B[i][j]=(double)k;
                        k++;
                }
}
void fill1D(double* x, int num_rows){
        int i,j;
        int k=0;
        for(i=0;i<num_rows;i++){
        	x[i]=(double)k;
                k++;
        }
}
