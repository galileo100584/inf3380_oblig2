#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEBUG 1

#ifdef DEBUG
    #define d printf
#else
    #define d
#endif


#include "../include/matrix_init.h"
#include "../include/matrix_math.h"
#include "../include/matrix_tools.h"

int main(){
	double **A;
	double **B;
	double **C;
	double *x;
	double *y;
	int a_num_rows;
	int b_num_rows;
	int a_num_cols;
	int b_num_cols;
	read_matrix_binaryformat ("small_matrix_a.bin", &A, &a_num_rows, &a_num_cols);	//100x50
	read_matrix_binaryformat ("small_matrix_b.bin", &B, &b_num_rows, &b_num_cols);	//50x100

	/*alloc(&A,&num_rows,&num_cols);
	alloc(&B,&num_rows,&num_cols);
	alloc(&C,&num_rows,&num_cols);
	fill(A,B,num_rows,num_cols);*/

	alloc(&C,&a_num_rows,&b_num_cols);
	alloc1D(x,a_num_cols,1);
	alloc1D(y,a_num_cols,1);
	//fill1D(x,a_num_cols);
	clock_t start = clock(), diff;
	//multiply(A,B,C,a_num_rows,b_num_cols);
	//multiplyVector(A,x,y);	//A*x = y
	diff = clock() - start;
	
	/*printf("A = \n");
	printMatrix(A,num_rows,num_cols);	
	printf("B = \n");
	printMatrix(B,num_rows,num_cols);
	printf("A x B = \n");
	printMatrix(C,num_rows,num_cols);*/

	printf("Multiply %d x %d matrix:\n",a_num_rows,b_num_cols);
	int msec = diff * 1000 / CLOCKS_PER_SEC;
	printf("Time taken %d seconds %d milliseconds\n", msec/1000, msec%1000);
	return 0;
}
